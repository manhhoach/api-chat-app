const mongoose = require('mongoose');
const messageModel = require('./../models/message')
const roomModel = require('./../models/room')
const { responseSuccess, responseError } = require('./../helper/response')
const { getPagingData, getPagination } = require('./../helper/pagination')

exports.getAll = async (req, res, next) => {
    try {
        //step 1: check user in room
        let room = await roomModel.findOne({
            _id: mongoose.Types.ObjectId(req.params.roomId),
            userIds: { $elemMatch: { _id: res.locals.user._id } }
        })
        if (room) {
            let { limit, offset } = getPagination(req.query.page_index, req.query.page_size)

            let messages = await messageModel.find({
                roomId: req.params.roomId
            }).skip(offset).limit(limit).sort('-createdAt')

            messages = messages.map(message => {
                let receivers = message.receivers.map(receiver => {
                    return {
                        _id: receiver._id._id,
                        name: receiver._id.name,
                        avatar: receiver._id.avatar
                    }
                })
                return {
                    ...message._doc,
                    receivers
                }
            })

            res.json(responseSuccess(messages))
        }
        else
            res.json(responseError("ROOM IS NOT EXISTS"))
    }
    catch (err) {
        next(err);
    }
}

exports.seenMessage = async (req, res, next) => {
    try {
        let message = await messageModel.findById(mongoose.Types.ObjectId(req.params.id))
        let receivers = message.receivers.map(ele => {
            if (ele._id._id.toString() === res.locals.user._id.toString())
                return {
                    _id: ele._id,
                    seen: true,
                    seenAt: new Date()
                }
            else
                return ele
        });
        message.receivers=receivers;
        await message.save()
        res.json(responseSuccess(message))
    }
    catch (err) {
        next(err);
    }
}
exports.create = async (req, res, next) => {
    try {
        let data = await messageModel.create({
            content: req.body.content,
            receiver: req.body.receiver,
            sender: res.locals.user._id,
            roomId: mongoose.Types.ObjectId(req.params.roomId)
        })
        res.json(responseSuccess(data))
    }
    catch (err) {
        next(err);
    }
}
exports.delete = async (req, res, next) => {
    try {
        
        let message=await messageModel.findOne({
            _id: mongoose.Types.ObjectId(req.params.id),
            sender: res.locals.user._id
        })
        if(message)
        {
            await messageModel.deleteOne({_id: mongoose.Types.ObjectId(req.params.id)})
            return res.json(responseSuccess("DELETE MESSAGE SUCCESSFULLY"));   
        }
        res.json(responseError("MESSAGE IS NOT EXISTS"));  
    }
    catch (err) {
        next(err);
    }
}