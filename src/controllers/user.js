const userModel = require('./../models/user');
const friendShipModel = require('./../models/friendShip');

const jwt = require('../middlewares/jwt');
const { responseSuccess, responseError } = require('./../helper/response')
const mongoose = require('mongoose');


exports.getAll = async (req, res, next) => {

}

exports.update = async (req, res, next) => {
    try {
        delete req.body.email;
        delete req.body.password;
        let user = await userModel.findByIdAndUpdate(res.locals.user._id, req.body, { new: true }).select('_id name avatar email')
        res.json(responseSuccess(user));
    }
    catch (err) {
        next(err);
    }
}

exports.register = async (req, res, next) => {
    try {
        const user = await userModel.create(req.body);
        res.json(responseSuccess(user));
    }
    catch (err) {
        next(err);
    }
}

exports.changePassword = async (req, res, next) => {
    try {
        const { oldPassword, newPassword } = req.body;
        let checkPassword = await res.locals.user.checkPassword(oldPassword, res.locals.user.password)
        if (checkPassword) {
            res.locals.user.password = newPassword;
            await res.locals.user.save();
            res.json(responseSuccess("CHANGE PASSWORD SUCCESSFULLY"));

        }
        else {
            res.json(responseError('Invalid password'));
        }

    }
    catch (err) {
        next(err);
    }
}



exports.login = async (req, res, next) => {
    try {
        let { email, password } = req.body;
        let user = await userModel.findOne({ email: email }).select('+password');
        if (user) {
            let checkPassword = await user.checkPassword(password, user.password)
            if (checkPassword) {
                let token = jwt.signToken(user._id);
                res.json(responseSuccess({
                    ...user._doc,
                    token
                }));

            }
            else {
                res.json(responseError('Invalid password'));
            }
        }
        else {
            res.json(responseError('Email is not exists'));
        }


    }
    catch (err) {
        next(err);

    }
}


exports.sendInvitation = async (req, res, next) => {
    try {
        let friendId = mongoose.Types.ObjectId(req.params.userId);
        let data = await friendShipModel.create({
            requesterId: res.locals.user._id,
            responderId: friendId
        })
        res.json(responseSuccess(data));
    }
    catch (err) {
        next(err);

    }
}

exports.changeStatus = async (req, res, next) => {
    try {
        let requestFriend = await friendShipModel.findById(mongoose.Types.ObjectId(req.params.id));
        const { status } = req.body;
        // both users can cancel invitation
        if (status === 'REJECTED') {
            await friendShipModel.deleteOne({ _id: requestFriend._id })
            return res.json(responseSuccess("CANCEL INVITATION SUCCESSFULLY"));
        }
        // if current status=PENDING, only responder can accept or reject invitation
        else if (requestFriend.status === 'PENDING' && res.locals.user._id.toString() === requestFriend.responderId.toString()) {
            await friendShipModel.updateOne({ _id: requestFriend._id }, { status: status })
            return res.json(responseSuccess("ACCEPT INVITATION SUCCESSFULLY"));
        }

        res.json(responseError("YOU CAN NOT UPDATE STATUS"));

    }
    catch (err) {
        next(err);

    }
}



exports.getAllFriendByStatus = async (req, res, next) => {
    try {
        let friends = [];
        if (req.query.status === 'ACCEPTED') {
            friends = await friendShipModel.find({
                $and: [
                    { status: 'ACCEPTED' },
                    { $or: [{ responderId: res.locals.user._id }, { requesterId: res.locals.user._id }] }
                ]

            });
            friends = friends.map(friend => {
                return friend.requesterId._id.toString() === res.locals.user._id.toString() ? friend.responderId : friend.requesterId;
            })
        }
        else if (req.query.status === 'PENDING') {
            friends = await friendShipModel.find({
                status: 'PENDING',
                responderId: res.locals.user._id
            })
            friends = friends.map(friend => {
                return friend.requesterId;
            })
        }
        res.json(responseSuccess(friends));
    }
    catch (err) {
        next(err);

    }
}

