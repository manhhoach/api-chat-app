const roomModel = require('./../models/room');
const messageModel = require('./../models/message');
const { responseSuccess, responseError } = require('./../helper/response')
const mongoose = require('mongoose');
const { getPagination } = require('./../helper/pagination')
const userModel = require('./../models/user')

const checkUserInRoom = async (roomId, userId) => {

    const room = await roomModel.findOne({ _id: roomId, userIds: userId })
    if (room)
        return room
    else
        return false

}

exports.getAll = async (req, res, next) => {
    try {
        let { limit, offset } = getPagination(req.query.page_index, req.query.page_size)
        let rooms = await roomModel.aggregate([
            { $match: { userIds: res.locals.user._id } }, // get room of user
            {
                $lookup: { // join with messages
                    from: 'messages', // name of collection
                    localField: '_id', //   _id in rooms
                    foreignField: 'roomId', // roomId in messages
                    as: 'message', //rename
                    pipeline: [{ $project: { _id: 1, content: 1, type: 1, sender: 1, createdAt: 1 } }]
                }
            },
            {
                // add message to rooms
                $unwind: '$message'
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'message.sender',
                    foreignField: '_id',
                    as: 'message.sender',
                    pipeline: [{ $project: { _id: 1, name: 1, avatar: 1 } }]

                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'userIds',
                    foreignField: '_id',
                    as: 'userIds',
                    pipeline: [{ $project: { _id: 1, name: 1, avatar: 1 } }]

                }
            },
            {
                $project: {
                    name: '$name',
                    userIds: '$userIds',
                    message: '$message',
                }
            },
            {
                $group: {
                    _id: '$_id', // group by rooms._id    
                    name: { "$first": "$name" },// add attribute in group=>get in project
                    userIds: { "$first": "$userIds" }, // add attribute in group=>get in project
                    message: { $last: '$message' } // using $last to retain only last message
                }
            },
            {
                $sort: { 'message.createdAt': -1 } // sort by messages.createdAt
            },
            { $skip: offset },
            { $limit: limit },

        ])
        rooms = rooms.map(ele => {
            if (ele.name)
                return ele;
            else {
                let name = [];
                ele.userIds.forEach(user => {
                    if (user._id.toString() !== res.locals.user._id.toString())
                        name.push(user.name)
                })
                ele.name = name.join(', ')
                return ele;
            }
        })


        res.json(responseSuccess(rooms))

    }
    catch (err) {
        console.log(err);
        next(err);
    }
}
exports.update = async (req, res, next) => {
    try {
        // version 1: update name
        // version 2: update user in room


        let room=await checkUserInRoom( mongoose.Types.ObjectId(req.params.id), res.locals.user._id)
        if(room)
        {
            room = await roomModel.findByIdAndUpdate(req.params.id, { name: req.body.name }, { new: true });
            res.json(responseSuccess(room))
        }
        else
        {
            res.json(responseError('ROOM IS NOT EXISTS'))
        }
            
    }
    catch (err) {
        next(err);
    }

}


module.exports.create = async (req, res, next) => {
    try {
        let userIds = [...req.body.userIds]
        userIds.push({ _id: res.locals.user._id.toString() })
        let availableRoom = await roomModel.findOne({ userIds: { $size: userIds.length, $all: [...userIds] } })

        if (!availableRoom) {
            availableRoom = await roomModel.create({
                userIds: userIds
            })
        }
        let message = await messageModel.create({
            ...req.body.message,
            roomId: availableRoom._id,
            sender: res.locals.user._id,
            receivers: req.body.userIds
        })
        res.json(responseSuccess(message))

    }
    catch (err) {
        next(err);
    }
}



exports.delete = async (req, res, next) => {
    try
    {   
        let room=await checkUserInRoom( mongoose.Types.ObjectId(req.params.id), res.locals.user._id)
        if(room)
        {
            room.userIds=room.userIds.filter(userId=>{
                return userId.toString() !== res.locals.user._id.toString()
            })
            await room.save()
           
            res.json(responseSuccess("LEAVE CONVERSATION SUCCESSFULLY"))
        }
        else
        {
            res.json(responseError('ROOM IS NOT EXISTS'))
        }
    }
    catch (err) {
            next(err);
    }
}