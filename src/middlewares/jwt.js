const jwt = require('jsonwebtoken')
const userModel = require('./../models/user');
const mongoose=require('mongoose')
const {responseError}=require('./../helper/response')

module.exports.signToken = (_id) => {
    return jwt.sign({
        _id
    }, process.env.SECRET_KEY, { expiresIn: '30d' });
}

module.exports.checkToken = async (req, res, next) => {
    try {
        let token = req.headers.authorization.split(' ')[1];
        let decoded = jwt.verify(token, process.env.SECRET_KEY);
        let user=await userModel.findById(mongoose.Types.ObjectId(decoded._id)).select('+password')
        if(user)
        {
            res.locals.user=user;
            return next();
        }
        res.json(responseError("User is not exists"));
    }
    catch (err) {
        res.json(responseError("Invalid or expired token provided!"));
    }
}

