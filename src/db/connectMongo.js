const mongoose = require('mongoose');

module.exports = async()=> {
    try{
        let DB=process.env.DB||'mongodb://127.0.0.1:27017/test-chat-app';
        await mongoose.connect(DB);
        console.log('Connect to database successfully')
    }
    catch(err){
        console.log(err);
    }
    
}

