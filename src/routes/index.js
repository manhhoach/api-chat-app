const express = require('express');
const router = express.Router();

const messageRouter = require('./message');
const roomRouter = require('./room');
const userRouter = require('./user');
const friendShipRouter=require('./friendShip')

router.use('/message', messageRouter);
router.use('/room', roomRouter);
router.use('/user', userRouter);
router.use('/friend-ship', friendShipRouter);

module.exports = router;
