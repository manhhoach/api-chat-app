const express= require('express');
const router= express.Router();
const friendShipController= require('./../controllers/friendShip');
const jwt=require('./../middlewares/jwt')

router.get('/',friendShipController.getAll);
router.post('/register',friendShipController.register); 
router.post('/login',friendShipController.login); 

router.use(jwt.checkToken)
router.patch('/change-password', friendShipController.changePassword); 
router.patch('/',friendShipController.update); 





module.exports = router;