const express= require('express');
const router= express.Router();
const messageController= require('./../controllers/message');
const jwt = require('./../middlewares/jwt')


router.use(jwt.checkToken)

router.get('/:roomId',messageController.getAll); // done
router.post('/:roomId',messageController.create); // done
router.patch('/seen/:id', messageController.seenMessage); // done
router.delete('/:id',messageController.delete);  // done


module.exports = router;