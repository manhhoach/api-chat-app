const express = require('express');
const router = express.Router();
const roomController = require('./../controllers/room');
const jwt = require('./../middlewares/jwt')


router.use(jwt.checkToken)

router.get('/', roomController.getAll); // sort by new message -> done
router.post('/', roomController.create); 
router.patch('/:id', roomController.update);
router.delete('/:id', roomController.delete);


module.exports = router;