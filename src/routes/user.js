const { application } = require('express');
const express= require('express');
const router= express.Router();
const userController= require('./../controllers/user');
const jwt=require('./../middlewares/jwt')

router.get('/',userController.getAll);
router.post('/register',userController.register); 
router.post('/login',userController.login); 

router.use(jwt.checkToken)
router.patch('/change-password', userController.changePassword); 
router.patch('/',userController.update);

router.post('/friend/send-invitation/:userId', userController.sendInvitation);  
router.patch('/friend/change-status/:id', userController.changeStatus); 


router.get('/friend', userController.getAllFriendByStatus); 



module.exports = router;