const express = require('express')
const app = express()
const path = require('path')
require('dotenv').config({});
const cors = require('cors')

const {responseError}=require('./helper/response')
const port = process.env.PORT || 5000;
const connectDB = require('./db/connectMongo')
const routes = require('./routes')

// socket io
const server = require('http').createServer(app)
const io = require("socket.io")(server);


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, './../public')))

app.use(routes)
app.get('/', (req, res) => {
  res.render('index.html')
})
app.use('*', (err, req, res, next) => {
  res.json(responseError(err))
})



io.on("connection", (socket) => {

  console.log("User connected to socket.io");

  socket.on('chat message', (message) => {
    io.emit('chat message', message);
  })

  socket.on("disconnect", () => {
    console.log("User Disconnected");
  });

});


connectDB();
server.listen(port, () => {
  console.log(`Server listening on http://localhost:${port}`)
})