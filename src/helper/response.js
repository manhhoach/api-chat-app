module.exports.responseSuccess = (data) =>{
    return {
        success: true,
        data: data
    }
}

module.exports.responseError= (error) =>{
    return {
        success: false,
        error: error.message ? error.message : error
    }
}
