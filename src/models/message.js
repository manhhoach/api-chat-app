const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['TEXT', 'IMAGE'],
        default: 'TEXT'
    },
    roomId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'room'
    },
    sender: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    receivers: [
        { 
            _id:{
                type: mongoose.Schema.Types.ObjectId, 
                ref: 'user' 
            },
            seen: {
                type: Boolean,
                default: false
            },
            seenAt :{
                type: Date
            }   
        }
    ]
}, {
    timestamps: true
})

messageSchema.pre(/^find/, function(next){
    this.populate({
        path:'sender',
        select:'name avatar'
    }).populate({
        path: 'receivers._id',
        select:'name avatar'
    })
    next()
})

const Message = mongoose.model('message', messageSchema);
module.exports = Message;