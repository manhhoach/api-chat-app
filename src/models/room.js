const mongoose = require('mongoose');
const userModel = require('./user')
const uniqueValidator = require('mongoose-unique-validator');

const roomSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    userIds: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }]
}, {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})



roomSchema.virtual('message', {
    ref: 'message',
    localField: '_id',
    foreignField: 'roomId',
    
})

const Room = mongoose.model('room', roomSchema);
module.exports = Room;