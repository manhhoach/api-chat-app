const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const friendShipSchema = new mongoose.Schema({
    requesterId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        index: true
    },
    responderId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user',
        index: true
    },
    status: {
        type: String,
        enum: ['PENDING', 'ACCEPTED', 'REJECTED'],
        default: 'PENDING'
    }

}, {
    timestamps: true
})


friendShipSchema.index({ requesterId: 1, responderId: 1 }, { unique: true });
friendShipSchema.plugin(uniqueValidator, {type : 'unique-error'});

friendShipSchema.pre(/^find/, function(next) {
    this.populate({path: 'requesterId responderId', select:'_id name avatar'});
    next()

})


const FriendShip = mongoose.model('friendShip', friendShipSchema);
module.exports = FriendShip;