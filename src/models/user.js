const mongoose = require('mongoose');
const bcryptjs = require('bcryptjs')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'You must have a name'],
    },
    avatar: {
        type: String,
        default: 'image/user_default.jpg'
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'You must have an email'],
        validate: {
            validator: function (email) {
                return /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(email)

            },
            message: 'You must enter a valid email'
        }
    },
    password: {
        type: String,
        required: true,
        select: false
    }
}, {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true }
})

userSchema.pre('save', function (next) { // dùng model.save thì cả create lẫn update đều chạy qua middleware này
    if (!this.isModified('password'))
        return next();

    const salt = bcryptjs.genSaltSync(10);
    this.password = bcryptjs.hashSync(this.password, salt);
    next()
})



userSchema.methods.checkPassword = async function (rawPassword, hashPassword) {
    return await bcryptjs.compare(rawPassword, hashPassword);
}

const User = mongoose.model('user', userSchema);
module.exports = User;